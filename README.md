## Firefox Cache Bug Testing App

This is a very stripped down version of my app, though I created it from a base
initial app and removed the things my live app also got rid of.

### Setup

1. Install Play Framework 1.5.3:
   https://downloads.typesafe.com/play/1.5.3/play-1.5.3.zip

1a. Unzip it somewhere and add it to your path or put at some nice easily-
    referenced path.

2. Install Python. I use 3.6.4

   If you already have a recent enough Python 3.x installed it will likely work
   fine as Play only uses Python for init scripts and other housekeeping bits,
   not the actual runtime.

3. Install Java. I use Java 11.0.2

   You should be able to use Java 8-11, not sure about anything after. This is
   used for the runtime of the app.

4. Check out this project.

5. Open a command prompt in the checkout directory and run `play secret`

### Testing for the Bug

1. Run `play run` in the check out directory

2. Open http://localhost:9000 in Firefox; wait for the page to load completely,
   inspect stuff, do whatever.

3. Close all localhost:9000 tabs.

4. Leave the app running in the background for 24h (currently haven't worked out
   a shorter duration)

5. After 24 hours open a new tab and visit http://localhost:9000
